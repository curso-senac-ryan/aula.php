<?php

#Array Unidimencional

$listaCompra= ['Arroz', 'Feijão', 'Banana', 'Detergente', 'Sabonete'];

echo "<pre>";
print_r($listaCompra);
echo "</pre>";

echo"<hr>";

var_dump($listaCompra);

echo "<hr>";

echo $listaCompra [2];

echo "<hr>";

echo $listaCompra [4]. ", ".$listaCompra[2];

echo "<hr>";

foreach ($listaCompra as $item) {
    echo $item. ", ";
    echo "<br>";
}

echo "<hr>";

#Array Associativo

$funcionario = [
     "nome" => "Ryan Marques", 
     "cargo" => "MEP",
     "idade" => 17,
     "salario" => 1500.50,
     "ativo" => true
];

var_dump($funcionario);

echo "<hr>";

echo $funcionario["cargo"];