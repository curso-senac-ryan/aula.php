<?php

$listaClientes = [
    'Edson Rodrigues',
    'Ana Maria',
    'Gustavo da Silva',
];

$limite = 10;
$tabuada = 5;
$contagem = 0;

echo '<h1>Trabalhando com Estrutura Laço de Repetição</h1>';

echo '<h2>Exemplo de While (enquanto...)</h2>';

while ($contagem <= 10) {
    echo $contagem++.' ';
}

echo '<hr>';
##################################
echo '<h2>Exemplo de do...while </h2>';

$contagem = 0;

do {
    echo $contagem++.' ';
} while ($contagem <= 10);

echo '<hr>';
##################################
echo '<h2>Exemplo de for (para)</h2>';

for ($contagem = 0; $contagem <=10; $contagem++) {
   echo $contagem.' ';
}

echo '<hr>';
##################################
echo '<h2>Exemplo de foreach</h2>';

//Ambiente de Desenvolvimento
echo var_dump ($listaClientes);

echo $listaClientes[1];

echo'<br><br>';

foreach ($listaClientes as $key => $nome){
    echo "A index/chave é: $key e o valor é $nome";
    echo'<br>';
}

echo'<br><br>';

foreach ($listaClientes as $nome){
    echo "valor é $nome";
    echo'<br>';
}