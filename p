function par_ou_impar ($numero)
{
   if($numero % 2 == 0){
       return "Número é Par";
   }

   return "Número é Ímpar";
}

function par_ou_impar_mod2 ($numero)
 {
     return ($numero % 2 == 1)? "Número é par" : "Número é Ímpar";
 }