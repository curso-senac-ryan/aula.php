<?php

function soma ($num1, $num2)
{
   $total = $num1 + $num2;

   return $total;
}

echo soma(10,5);

echo "<hr>";

function subtração ($num3, $num4)
{
   $total = $num3 - $num4;

   return $total;
}

   echo subtração(11,5);

echo "<hr>";

function multiplicação ($num5, $num6)
{
    $total = $num5 * $num6;

    return $total;
} 

   echo multiplicação(12,6);

echo "<hr>";

function divisão ($num7, $num8) 
{
    $total = $num7 / $num8;
    
    return $total;
}
   
echo divisão(3,1);

echo "<hr>";

function par_ou_impar ($numero)
{
   if($numero % 2 == 0){
       return "Número é Par";
   }

   return "Número é Ímpar";
}

function par_ou_impar_mod2 ($numero)
 {
     return ($numero % 2 == 1)? "Número é par" : "Número é Ímpar";
 }

 echo par_ou_impar(10);

 /////////////////////////////////

 echo "<hr>";

 function geradorSenhaComFor($senhaInicial = 1, $senhaFinal = 10)
 {
      for($contador = $senhaInicial; $contador <= $senhaFinal; $contador++){
      echo $contador. "-";
     }
 }

 geradorSenhaComFor(10,20);

 echo "<hr>";

 function geradorSenhaComWhile($senhaInicial = 1, $senhaFinal = 10)
 {
     $contador =$senhaInicial;

     while($contador <= $senhaFinal){
         echo $contador. "-";
         $contador++;
     }
    
}

 geradorSenhaComWhile(15,20);