<?php

$nome = 'Ryan Marques';
$idade = 18;
$email = 'ryannmarques617@gmail.com';
$senha = '12345678';
$cursos = ['PHP', 'HTML', 'CSS'];

echo '<h1>Trabalhando com Estrutura condicional</h1>';

echo '<h2>Exemplo de if (se...)</h2>';

if ($idade >= 18) {
    echo "O usuario $nome é maior de idade";
}

######################################

echo '<hr>';

echo "<h2>Exemplo de if Ternário</h2>";

echo ($idade >= 18) ? "Maior de Idade" : "Menor de Idade";

######################################

echo '<hr>';

echo "<h2>Exemplo de if e else</h2>";

if ($email == "ryannmarques617@gmail.com" && $senha == "12345678") {
    echo "Usuario Logado";
} else {
    echo "Usuario ou Senha Invalída";
}
echo '<br>';


#######################################

echo "exemplo 2";

echo '<br>';

if ($email == 'ryannmarques17@gmail.com') {
    if ($senha == '12345670') {
        echo "Usuario Logado";
    } else {
        echo "Usuario ou Senha Invalida";
    }
} else {
    echo "Usuario ou Senha Invalida";
}

######################################
echo '<hr>';

echo "<h2>Exemplo de Multiplicas Condições</h2>";

$num1 = 10;
$num2 = 20;

if ($num1 == $num2) {
    echo 'os números são iguais';
} elseif ($num1 > $num2) {
    echo 'O número 1 é maior que o número 2';
} else {
    echo 'O número 2 é maior que o número 1';
}

######################################
echo '<hr>';

echo "<h2>Exemplo de GET</h2>";

$menu = $_GET['menu'] ?? "Home";

switch(strtolower($menu)) {
    case "Home":
        echo "Página Principal";
        break;
    case "Empresa":
        break;
    case "Página Empresa":
    case "Produtos":
        break;
    case "Página Produtos":
    case "Contatos":
        break;
    case "Página Contato":
    default:
        echo "Página Erro 404";
}
