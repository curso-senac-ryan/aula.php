<?php
$nome = 'Ryan Marques';
$nacionalidade = 'brasileiro';
$cargo = 'Docente';
$cpf = '222.333.444.33';
$rg = '34.111.444.X';
$cnpj = '51.757.388/0001‐00';
$endereço = 'R.Paraíba,125 ‐ Marília,SP,17509‐060';
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Teste</title>
</head>
<body>

    <h1>Declaração de local de trabalho</h1>  

    <p>Eu, <?=$nome?>, <?=$nacionalidade?>, <?=$cargo?>, inscrito(a) no CPF sob o nº <?=$cpf?> e no RG
    nº <?=$rg?>, declaro para os devidos fins que possuo vínculo empregatício com a empresa
    Senac inscrita no CNPJ sob o nº <?=$cnpj?>, localizada à <?=$endereço?>
    <br>
    <br>
    Por ser expressão da verdade, firmo a presente para efeitos legais.
    <br>
    <br>
    Marília – SP, 15 de Setembro de 2022</php>
    <br>
    <br>
    Ryan Marques

</body>
</html>